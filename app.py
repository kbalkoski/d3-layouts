from flask import Flask
from flask import render_template
import pandas as pd

app = Flask(__name__)


@app.route("/")
def index():
    return render_template("bubble.html")


@app.route("/bubble")
def bubble():
    return render_template("bubble.html")


@app.route("/donut")
def donut():
    return render_template("Donut.html")


@app.route("/histogram")
def histo():
    return render_template("histogram.html")


@app.route("/pack")
def pack():
    return render_template("pack.html")


@app.route("/tree")
def tree_html():
    return render_template("tree.html")

if __name__ == "__main__":
    app.run(host='0.0.0.0',port=5000,debug=True) # Mac
    # app.run(host='127.0.0.1',port=80,debug=True) # Windows
